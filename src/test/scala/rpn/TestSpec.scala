package rpn

import org.scalatest._

class TestSpec extends FlatSpec with Matchers {

    "A RPN" should "calculate (1 1 +) = 2" in {
        RPN.rpn("1 1 +") should be(2)
    }

    "A RPN" should "calculate (1 -2 +) = -1" in {
        RPN.rpn("1 -2 +") should be(-1)
    }

    "A RPN" should "calculate (2 1 -) = 1" in {
        RPN.rpn("2 1 -") should be(1)
    }

    //    "A RPN" should "calculate (2 2 *) = 4" in {
    //        RPN.rpn("2 2 *") should be(4)
    //    }
    //
    //    "A RPN" should "calculate (2 2 /) = 1" in {
    //        RPN.rpn("2 2 /") should be(1)
    //    }
}

