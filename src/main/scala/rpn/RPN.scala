package rpn

object RPN {

    def rpn(str: String) = {

        str.split(' ').toList.foldLeft(List[Int]())(
            (list, token) ⇒ (list, token) match {
                case (x :: y :: zs, "+") ⇒ (y + x) :: zs
                case (x :: y :: zs, "-") ⇒ (y - x) :: zs
                //                case (x :: y :: zs, "*") ⇒ (y * x) :: zs
                //                case (x :: y :: zs, "/") ⇒ (y / x) :: zs
                case (_, _)              ⇒ token.toInt :: list
            }).head

    }
}